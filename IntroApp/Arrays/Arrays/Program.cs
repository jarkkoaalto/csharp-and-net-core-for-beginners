﻿using System;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = {"Bo","Li","An"};
            // string[] names = new string[2];
            // names[0] = "Bo";
            // names[1] = "Li";
            // Console.WriteLine(names[0]);
            // Console.WriteLine(names[1]);

            //PrintFirst(names);
            //PrintLast(names);

            foreach(string name in names)
            {
                Console.WriteLine(name);
            }

            for(int i = 0; i<names.Length; i++)
            {
                Console.WriteLine(names[i]);
            }
               
        }

        static void PrintFirst(string[] values)
        {
            Console.WriteLine(values[0]);
        }

        static void PrintLast(string[] values)
        {
            Console.WriteLine(values[values.Length -1]);
        }
    }
    
}
