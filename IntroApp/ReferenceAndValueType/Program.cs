﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceAndValueType
{
    class Program
    {


        static void Main(string[] args)
        {
            //  House house = new House(10, "Yellow");
            //  DoStuffToHouse(house);

            //  Console.WriteLine(house.NumberOfWindows);
            //  Console.WriteLine(house.Color);

            House house = new House(1,"Yellow");
            house = null;
            Console.WriteLine(house);

            // Nullable<int> i = null;
            int? i = null;

            StringBuilder s = new StringBuilder("test");
            for(int j=0; j < 100; j++)
            {
                s.Append("!");  
            }
            Console.WriteLine(s);



        }

        static void DoStuffToHouse(House house)
        {
            house.NumberOfWindows--;
            house.Color = "Black and White";
        }
    }
}
