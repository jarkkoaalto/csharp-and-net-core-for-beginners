﻿using System;

namespace Section08
{
    /*
     * This section
     * - method definition
     * - parameters and arguments
     * - return type
     * - returning tuples
     * - precedence
     * - reading user input
     * - converting user input
     * 
     */
    class Program
    {
        static void Main(string[] args)
        {
            string FormattedInfo = FormatInfo("Jon", "Doe", 24);
            Console.WriteLine(FormattedInfo);


            var (name, ages) = GetPersonInfo();
            Console.WriteLine(name);
            Console.WriteLine(ages);
        }

        static string FormatInfo(string firstName, string lastName, int age)
        {
            return $" Name: {firstName} {lastName} Age: {age}";
        }

        static string (string name, int age) GetPersonInfo()
        {
            Console.WriteLine("Enter your name:");
            string name = Console.ReadLine();
            int age = int.Parse(Console.ReadLine());
            return (name, age);
        }

       
    }
}
