﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(Divide(10, 0));
            }
            catch (Exception)
            { 
                Console.WriteLine("SOmething gone wrong ...");
            }
        
        }

        static int Divide(int a, int b)
        {
    
            if (b == 0)
                throw new ArgumentException("Argument must not be zero", nameof(b));

            return a / b;
  
        }
    }
}
