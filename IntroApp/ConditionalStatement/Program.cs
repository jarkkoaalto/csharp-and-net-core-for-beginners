﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionalStatement
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 1. If Statements
             * 2. Switch
             * 
             */

            string input = "";
            Console.WriteLine("Enter y or n:");
            input = Console.ReadLine();
            if(input == "y")
            {
                Console.WriteLine("You entered yes");
            }
            else if(input == "n")
            {
                Console.WriteLine("You entered No");
            }
            else
            {
                Console.WriteLine("You entered something else");
            }


            switch (input)
            {
                case "y": Console.WriteLine("Yes");break;
                case "n": Console.WriteLine("No");break;
                default: Console.WriteLine("Something else..."); break;
            }


            bool isYes = true;
            int age = 30;
            if(isYes && age > 40)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }

            switch (age)
            {
                case 10: Console.WriteLine("10");break;
                case 20: Console.WriteLine("20");break;
                default: Console.WriteLine("Something else"); break;
            }

        }
    }
}
