﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proprties
{
    class Person
    {
        public Person(int age)
        {
            // Age = age;
        }
        /*
        private int age;

        public int Age
        {
            get { return age; }
            set
            {
                if (value > 0)
                    age = value;
                else
                    age = 0;
            }
        }
        */

        // These is exatly same thing
        //public int Age { get; set; }
        //  public int Age { get; private set; }
        public int Age { get; } = 30;
 
    }
}
