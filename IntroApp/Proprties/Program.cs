﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proprties
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person(10);

            // Set
            // person.Age = 5; NO LONGER PUBLIC
         
            // Get
            Console.WriteLine(person.Age);
        }
    }
}
