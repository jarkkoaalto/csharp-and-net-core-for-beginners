﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Section12
{
    class Program
    {
        static void Main(string[] args)
        {
            Person obj = new Person("Bo");
            Person obj1 = new Person("Li");

            obj.age = 33;
            obj1.age = 55;

            Console.WriteLine(obj.name);
            Console.WriteLine(obj.age);
            Console.WriteLine(obj1.name);
            Console.WriteLine(obj1.age);
        }
    }
}
