﻿using System;

namespace Section04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(args.Length);
            Console.WriteLine("Hello World !");
            Console.WriteLine("GoodBye World !");
            Foo();
            Foo("Yasss");
        }

        static void Foo()
        {
            Console.WriteLine("In Foo ...");
        }

        static void Foo(string name)
        {
            Console.WriteLine("In foo ... " + name);
        }
    }
}
