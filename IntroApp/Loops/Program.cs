﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
    class Program
    {
        static void Main(string[] args)
        {
            bool repeat = true;

            do
            {
                Console.WriteLine("In loop ... Repeat?");
            } while (repeat = Console.ReadLine() == "y");

            /**
            while (repeat)
            {
                Console.WriteLine("In loop ... Repeat?");
                repeat = Console.ReadLine() == "y";
            }
        */

            for(int i=0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            foreach(var item in "Test")
            {
                Console.WriteLine(item);
            }
        }
    }
}
