﻿using Section06;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanGame
{
    class Program
    {
        static string correctWord;
        static char[] letters;
        static Player player;

        static void Main(string[] args)
        {
            try
            {
                StartGame();
                PlayGame();
                EndGame();
            }
            catch(Exception)
            {
                Console.WriteLine("Oops, Something gome wrong");
            }
        }

        private static void StartGame()
        {
            string[] words;
            try { 
            words = File.ReadAllLines(@"C:\\Users\\Jarkko\\Desktop\\Udemy\\GITREPOS\\csharp-and-net-core-for-beginners\\IntroApp\\FileManagement\\words.txt");
            }
            catch
            {
                words = new string[] { "hangman", "superman", "naked" };
            }
            Random random = new Random();
            correctWord = words[random.Next(0, words.Length)];

            letters = new char[correctWord.Length];
            for(int i = 0; i < correctWord.Length; i++)
            {
                letters[i] = '_';
            }
            AskForUserName();
        }

        static void AskForUserName()
        {
            Console.WriteLine("Enter your name :");
            string input = Console.ReadLine();
            if(input.Length >= 2)
            {
                // The user must entered valid name
                player = new Player(input);
            }
            else
            {
                // The user entered invalid name
                AskForUserName();
            }
        }


        private static void PlayGame()
        {
            do
            {
                Console.Clear();
                displayMaskedWord();
                char guessedLetter = AskForLetter();
                CheckLetter(guessedLetter);
            } while (correctWord != new string(letters));

            Console.Clear();
        }

        public static void CheckLetter(char guessedLetter)
        {
            for(int i=0; i<correctWord.Length; i++)
            {
                if(guessedLetter == correctWord[i])
                {
                    letters[i] = guessedLetter;
                    player.Score++;
                }
            }
        }


        static void displayMaskedWord()
        {
            foreach (char c in letters)
            {
                Console.Write(c);
            }

            Console.WriteLine();
        }

        static char AskForLetter()
        {
            string input;
            do
            {
                Console.WriteLine("Enter a letter:");
                input = Console.ReadLine();
            }
            while (input.Length != 1);
            
            var letter = input[0];

            if (!player.GuessedLetters.Contains(letter))
                player.GuessedLetters.Add(letter);

            return letter;
        }

        private static void EndGame()
        {
            Console.WriteLine("Congrats! ");
            Console.WriteLine($"Thanks to playint {player.Name}");
            Console.WriteLine($"Guissess: {player.GuessedLetters.Count} Score: {player.Score}");
        }
        
    }
}
