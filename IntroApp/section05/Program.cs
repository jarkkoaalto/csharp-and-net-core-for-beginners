﻿using System;

namespace section05
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            // Console.WriteLine("Hi");

            Foo();
            Foo("There");
            Person.CelebrateBirthday();
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("! " + i);
            }
        }

        private static void Foo(string v)
        {
            Console.WriteLine("Hi " + v);
        }

        private static void Foo()
        {
            Console.WriteLine("With out method ....");
        }


    }
}
