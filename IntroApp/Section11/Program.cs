﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Section11
{
    class Program
    {
        static void Main(string[] args)
        {
            // string[] name = new string[3]; if we don't know how many element

            var names = new List<string>();
            names.Add("Jo");
            names.Add("han");
            names.Add("nes");

            foreach(var name in names)
            {
                Console.WriteLine(name);
            }

            Console.WriteLine(names[0]); // bo
            Console.WriteLine(names.Count); // 3

            Console.WriteLine("******************");

            var queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            
            foreach(var item in queue)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("******************");

            var stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            Console.WriteLine(stack.Pop());

            Console.WriteLine("******************");

            var people = new Dictionary<string, int>();
            people.Add("Tommi", 22);
            people.Add("Wasp", 33);
            people.Add("An", 54);

            Console.WriteLine(people["Tommi"]);
        }
    }
}
